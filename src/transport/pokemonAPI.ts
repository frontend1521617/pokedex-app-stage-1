import { IPokemon, IPokemonAPI, IPokemonListItem } from '@/model/model'

export class PokemonAPI implements IPokemonAPI {
	baseURL = 'https://pokeapi.co/api/v2/pokemon/'

	constructor(public query = '?limit=20&offset=0') {}

	async fetchPokemon(pokemonListItem: IPokemonListItem): Promise<IPokemon> {
		let response = await fetch(pokemonListItem.url)
		let pokemon = await response.json()
		return {
			id: pokemon.id,
			name: pokemon.name,
			avatar: pokemon.sprites.front_default,
			catch: false,
		}
	}

	async fetchPokemonList(): Promise<IPokemon[]> {
		let response = await fetch(this.baseURL + this.query)
		let page = await response.json()
		let pokemonList: IPokemon[] = await Promise.all(
			page.results.map(this.fetchPokemon)
		)
		return pokemonList
	}
}
//async (item: IPokemonListItem) => await fetchPokemon(item)
