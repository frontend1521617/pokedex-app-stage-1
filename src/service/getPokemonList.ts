import { IPokemon, IPokemonList } from '@/model/model'
import { store } from '@/store/store'
import { PokemonAPI } from '@/transport/pokemonAPI'

export async function getPokemonList(
	pageNumber: number
): Promise<IPokemonList> {
	let api = new PokemonAPI(`?limit=${12}&offset=${(pageNumber - 1) * 12}`)
	let pokemonList: IPokemonList = {}
	if (store.getPageFromStore(pageNumber)) {
		pokemonList = store.getPageFromStore(pageNumber)
	} else {
		let page: IPokemon[] = await api.fetchPokemonList()
		page.forEach((pokemon: IPokemon) => {
			pokemonList[pokemon.id] = pokemon
		})
		store.pushPageToStore(pokemonList, pageNumber)
	}
	return pokemonList
}
