import { store } from '@/store/store'
export function catchPokemon(pageNumber: number) {
	let buttons = document.querySelectorAll('button')
	buttons.forEach(button => {
		button.addEventListener('click', () => {
			let id = Number(button.getAttribute('id'))
			store.pages[pageNumber][id].catch = true
			button.setAttribute('disabled', '')
			button.textContent = 'Caught'
		})
	})
}
