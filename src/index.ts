import { renderApp } from './view/view'
const root = document.getElementById('root')

if (!root) {
	throw new Error('root not found')
}

renderApp(root)
