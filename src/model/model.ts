export interface IPokemon {
	id: number
	name: string
	avatar: string
	catch: boolean
}
export interface IPokemonList {
	[id: number]: IPokemon
}

export interface IPokemonAPI {
	baseURL: string
	fetchPokemon(pokemonListItem: IPokemonListItem): Promise<IPokemon>
	fetchPokemonList(): Promise<IPokemon[]>
}
export interface IPokemonListItem {
	name: string
	url: string
}

export interface IPages {
	[key: number]: IPokemonList
}

export interface IStore {
	pages: IPages
	currentPage: number
}
