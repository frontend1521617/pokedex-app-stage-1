import { catchPokemon } from '@/service/catchPokemon'
import { getPokemonList } from '@/service/getPokemonList'
import { setPageNumber } from '@/service/pagination'
import { store } from '@/store/store'
import '../view/view.scss'
import { renderCardsList } from './cardList/cardList'
function renderHeader() {
	return `
	<img class='logo' src="./logo.png" alt="logo">
	`
}

export async function renderApp(root: HTMLElement) {
	const pageNumber = 1
	const pokemonList = await getPokemonList(pageNumber)

	let header = document.createElement('header')
	let main = document.createElement('main')
	root.append(header, main)
	header.insertAdjacentHTML('beforeend', renderHeader())
	main.append(renderCardsList(pokemonList))

	catchPokemon(pageNumber)
	setPageNumber()
	console.log(store.currentPage)
}
