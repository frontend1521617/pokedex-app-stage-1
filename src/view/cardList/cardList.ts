import { IPokemon, IPokemonList } from '@/model/model'
import { renderCard } from '../card/card'
import styles from './cardList.module.scss'
export function renderCardsList(pokemonList: IPokemonList) {
	let ul = document.createElement('ul')
	ul.classList.add(`${styles.cardList}`)
	Object.values(pokemonList).forEach((pokemon: IPokemon) => {
		ul.insertAdjacentHTML('beforeend', renderCard(pokemon))
	})
	return ul
}
