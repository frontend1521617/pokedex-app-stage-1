import { IPokemon } from '@/model/model'
import styles from './card.module.scss'

export function renderCard(pokemon: IPokemon) {
	return `
		<li class="${styles.card}">
			<h2>id: ${pokemon.id} ${pokemon.name}</h2>
			<img src="${pokemon.avatar}" alt="${pokemon.name}">
			<button id='${pokemon.id}' ${pokemon.catch ? 'disabled' : ''} class='${
		styles.btn
	}' >${!pokemon.catch ? 'Catch me!' : 'Caught'}</button>
		</li>
	`
}
