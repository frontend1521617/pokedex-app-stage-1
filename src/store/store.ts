import { IPages, IPokemonList, IStore } from '@/model/model'
class Store implements IStore {
	pages: IPages = {}
	currentPage: number = 1

	getPageFromStore(pageNumber: number): IPokemonList {
		return this.pages[pageNumber]
	}
	pushPageToStore(pokemonList: IPokemonList, pageNumber: number) {
		this.pages[pageNumber] = pokemonList
	}

	setPage(pageNumber: number) {
		this.currentPage = pageNumber
	}
}
export const store = new Store()
